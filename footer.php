<?php

$telegram = 'https://t.me/nemondo0';
$twitter = 'https://twitter.com/nemondoglobal ';
$facebook = 'https://www.facebook.com/Nemondo-Global-156064444983581/';

?>

<footer class="nem-footer">
    <div class="container">
        <div class="nem-footer-line row align-items-center">
            <div class="col-lg-3 col-md-6">
                <a href="index.php" class="nem-logo">
                    <img src="/images/logo_header_1.5x.svg" alt="Nemondo logo" class="img-responsive">
                </a>
            </div>
            <div class="col-md-6">
                <div class="nem-footer-links">
                    <a href="/terms.php" class="nem-link">Terms of Service</a>
                    <a href="/privacy.php" class="nem-link">Privacy Policy</a>
                </div>
            </div>
            <div class="col-lg-3 col-md-12">
                <div class="nem-social">
                    <!--<a href="<?/*= $telegram */?>" class="nem-social-icon" target="_blank">
                        <div class="hexagon"></div>
                        <div class="icon icon-telegram"></div>
                    </a>-->
                    <a href="<?= $facebook ?>" class="nem-social-icon" target="_blank">
                        <div class="hexagon"></div>
                        <div class="icon fa fa-facebook"></div>
                    </a>
                    <a href="<?= $twitter ?>" class="nem-social-icon" target="_blank">
                        <div class="hexagon"></div>
                        <div class="icon fa fa-twitter"></div>
                    </a>
                </div>
            </div>
        </div>
        <div class="nem-footer-line row align-items-end">
            <div class="col-md-8">
                <div class="nem-address-list">
                    <p><i class="fa fa-map-marker"></i> Ahtri 12, Tallinn 10151 Estonia</p>
                    <p><i class="fa fa-map-marker"></i> 71-75 Shelton St. Covent Garden, London WC2H9JQ</p>
                </div>
            </div>
            <div class="col-md-4">
                <p class="copy text-right">&copy; Nemondo OÜ <?= date("Y")?></p>
            </div>
        </div>
    </div>
</footer>

<!--<div id="animatedModal">-->
<!--    <div class="close-animatedModal">-->
<!--        <i class="fa fa-close"></i>-->
<!--    </div>-->
<!---->
<!--    <div class="modal-content">-->
<!--        <iframe-->
<!--                src="https://changelly.com/widget/v1?auth=email&from=BTC&to=ETH&merchant_id=6fd913a1fbbd&address=&amount=1&ref_id=6fd913a1fbbd&color=412781"-->
<!--                width="600"-->
<!--                height="500"-->
<!--                class="changelly"-->
<!--                scrolling="no"-->
<!--                style="overflow-y: hidden; border: none"-->
<!--        >-->
<!--            Can't load widget-->
<!--        </iframe>-->
<!--    </div>-->
<!--</div>-->


<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="js/libs.js"></script>
<script src="js/scripts.js"></script>
<script>
    window.intercomSettings = {
        app_id: "f7vjv29i"
    };
</script>
<script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/f7vjv29i';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()</script>
