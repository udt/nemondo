<?php

$changelly_buy_sell = 'https://changelly.com/widget/v1?auth=email&from=BTC&to=ETH&merchant_id=6fd913a1fbbd&address=&amount=1&ref_id=6fd913a1fbbd&color=00cf70';
$changelly_exchange  = 'https://changelly.com/widget/v1?auth=email&from=BTC&to=ETH&merchant_id=6fd913a1fbbd&address=&amount=1&ref_id=6fd913a1fbbd&color=00cf70';

?>

<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no">

    <meta name="description" content="">
    <meta name="keywords" content="">

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/apple-touch-icon-144x144.png" />
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="images/apple-touch-icon-152x152.png" />

    <link rel="icon" type="image/png" href="images/favicon-32x32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="images/favicon-16x16.png" sizes="16x16" />

    <link rel="icon" type="image/x-icon" href="images/favicon.ico" />

    <script src="https://www.google.com/recaptcha/api.js?hl=en" async defer></script>

    <title>Nemondo | The World Is Yours</title>

    <link rel="stylesheet" href="css/libs.css">
<!--    <link rel="stylesheet" href="https://changelly.com/widget.css"/>-->
    <link rel="stylesheet" href="css/style.css">
</head>
<body>

<section class="nem-s nem-s--purple">
    <div class="container nem-banner-container">
        <a href="#" class="nem-logo">
            <img src="/images/logo_header_1.5x.svg" alt="Nemondo logo" class="img-responsive">
        </a>
        <div class="nem-banner row align-items-center">
            <div class="col-lg-6">
                <div class="nem-banner-content">
                    <h1>Two-Sided Liquidity Provider of Digital Assets</h1>
                    <p>The Nemondo OTC trading desk is a leading two-sided liquidity provider for the cryptocurrency markets, offering a streamlined experience for private high net worth individuals and institutions. We support all major cryptocurrencies.</p>
                    <!--<div class="nem-btn-section">
                        <a href="#animatedModal" class="nem-btn js_modal"><span class="gradient"></span>Buy/Sell Instantly</a>
                        <a href="#animatedModal" class="nem-btn js_modal"><span class="gradient"></span>Exchange Instantly</a>
                    </div>-->
                </div>
            </div>
            <div class="col-lg-6">
                <div class="nem-illustration">
                    <img src="/images/pyramid.png" alt="Pyramid" class="pyramid">
                    <img src="/images/ball-21x21.png" alt="" class="ball ball-1">
                    <img src="/images/ball-14x14.png" alt="" class="ball ball-2">
                    <img src="/images/ball-12x12.png" alt="" class="ball ball-3">
                    <img src="/images/ball-10x10.png" alt="" class="ball ball-4">
                    <img src="/images/ball-9x9.png" alt="" class="ball ball-5">
                    <img src="/images/ball-7x7.png" alt="" class="ball ball-6">
                    <img src="/images/ball-5x5.png" alt="" class="ball ball-7">

                    <img src="/images/ball-13x13.png" alt="" class="ball ball-main ball-main-left">
                    <img src="/images/ball-13x13.png" alt="" class="ball ball-main ball-main-centered">
                    <img src="/images/ball-13x13.png" alt="" class="ball ball-main ball-main-right">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="nem-s nem-s-middle">
    <div class="container">
        <div class="row">
					<?php
					$hexagons = [
						'object1' => [
							'image' => 'best-service.svg',
							'text'  => '24/7 Personalized Support',
						],
						'object2' => [
							'image' => 'pricing.svg',
							'text'  => 'Accurate Pricing & Speedy Settlement'
						],
						'object3' => [
							'image' => 'liquidity.svg',
							'text'  => 'Deep Pool of Liquidity'
						]
					];
					$i = 1;
					foreach ($hexagons as $hex => $index) : ?>
              <div class="col-md-4 hex-wrap hex-elem-<?= $i++ ?>">
                  <img src="/images/<?= $index['image']; ?>" alt="Best service in market" class="img-responsive">
                  <!--<div class="hex-wrap hex-elem-<?/*= $i++ */?>">
						<?php /*for($x=0;$x<3;$x++) : */?>
                            <div class="deco-dot deco-dot-<?/*= $x */?>"></div>
						<?php /*endfor; */?>
                      <div class="hex-deco hex-deco-4"></div>
                      <div class="hex-deco hex-deco-3"></div>
                      <div class="hex-deco hex-deco-2"></div>
                      <div class="hex-deco hex-deco-1"></div>
                      <div class="hex-deco-main"></div>
                      <img src="/images/<?/*= $index['image']; */?>" alt="Best service in market">
                  </div>-->
                  <div class="h4 text-center"><?= $index['text']; ?></div>
              </div>
					<?php endforeach;
					?>
        </div>
    </div>
</section>

<section class="nem-s nem-s--white nem-s--last">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2">
                <h2 class="text-center">Trades Over 100k? <br> Contact Us</h2>
                <p>To learn more and apply for eligibility, please fill the registration form below.</p>
                <form class="nem-f" id="nem-form" action="send.php" method="POST">
                    <div class="nem-f-group form-group">
                        <div class="nem-f-control">
                            <input type="text" name="name" class="nem-f-elem form-control" placeholder="Full name" required id="name">
                            <span class="helper"></span>
                            <div class="help-block nem-help-block">This field is required</div>
                        </div>
                    </div>
                    <div class="nem-f-group form-group">
                        <div class="nem-f-control">
                            <input type="text" name="org" class="nem-f-elem form-control" placeholder="Organization (If applicable)" id="org">
                            <span class="helper"></span>
                            <div class="help-block nem-help-block">This field is required</div>
                        </div>
                    </div>
                    <div class="nem-f-group form-group">
                        <div class="nem-f-control">
                            <input type="email" name="email" class="nem-f-elem form-control" placeholder="E-mail address" required id="email">
                            <span class="helper"></span>
                            <div class="help-block nem-help-block">Incorrect e-mail address</div>
                        </div>
                    </div>

                    <div class="g-recaptcha" data-sitekey="6Lf3wFgUAAAAABkegJULkIHBfEtyn0AYLipf83i0"></div>
                    <br/>

                    <button type="submit" class="nem-btn">
                        <span class="gradient"></span>
                        Get in Touch
                    </button>
                    <div class="nem-result-info"></div>
                </form>
                <p>The eligibility terms for all applicants include a minimum order of 100K equivalent, obligatory KYC (know your customer) procedure and other requirements.</p>
            </div>
        </div>
    </div>
</section>

<?php include 'footer.php'; ?>

</body>
</html>