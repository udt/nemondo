<?php include "header.php"; ?>

<header class="nem-header">
    <div class="container">
        <div class="row align-content-center justify-content-between">
            <div class="col-sm-6 col-xs-12">
                <a href="index.php" class="nem-logo">
                    <img src="/images/logo_header_1.5x.svg" alt="Nemondo logo" class="img-responsive">
                </a>
            </div>
            <div class="col-sm-6 col-xs-12 text-right">
                <nav>
                    <ul class="nem-nav">
                        <li>
                            <a href="index.php" class="nem-link">Home</a>
                        </li>
                        <li>
                            <a href="terms.php" class="nem-link">Terms of Service</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>

<main class="nem-main nem-page">
	<div class="container">
		<h2 class="text-center">Privacy Policy</h2>

		<p>This Privacy Policy describes how Nemondo OÜ (“Nemondo” or “we”) processes (e.g. collects, uses, stores, shares) and protects your data. This policy is incorporated by reference into the Nemondo User Agreement and has the binding force of that contract. By using Nemondo Services, you consent to the data practices prescribed in this statement. </p>

		<p>Throughout this policy, we use the term “personal data”, which means any information relating to an identified or identifiable natural person (“data subject”); an identifiable natural person is one who can be identified, directly or indirectly, in particular by reference to an identifier such as a name, an identification number, location data, an online identifier or to one or more factors specific to the physical, physiological, genetic, mental, economic, cultural or social identity of that natural person.</p>

		<h4 class="d-sm-block">Data We Process and the Purpose of It</h4>
        <h4 class="visible-xs">Data We Process <br> and the Purpose of It</h4>

		<p>We process your personal data in order to provide our services to you.<br> If you contract with Nemondo for Services, Nemondo, or its affiliates acting on its behalf may process the following types of data:</p>

		<ol>
			<li>contact information - your name, address, phone, email address, and other similar data like Skype ID, Telegram ID (if you contact us we would like to be able to respond to your inquiry);</li>
			<li>financial information - if you are transferring payment to Nemondo or receiving assets from Nemondo it may be necessary for Nemondo to collect account information for your account; we also collect bank wire details that we keep on file when we send payment as well client’s cryptocurrency wallet receive code;</li>
			<li>consistent with the requirements of the Laws of Estonia, when you begin to do business with Nemondo, Nemondo will process the data requested in its know your customer (“KYC”) procedures. This is necessary in order to protect both you and Nemondo. </li>
		</ol>

		<p>During Nemondo KYC procedure we process the following data:</p>

		<ol>
			<li>copy of identification document (which is passport, driver licence or other government issued ID with photo) of a client, who is a natural person;</li>
			<li>registration document of a client, who is a legal person, and names of the major shareholders or representatives of the client;</li>
			<li>copy of identification document (which is passport, driver licence or other government issued ID with photo) at least of one of the major shareholder or representative of a client, who is a legal person;</li>
			<li>for proving client’s address, we ask a client, who is a natural person, to provide us a utility bill that matches with client’s name;</li>
			<li>for proving client’s address, we ask a client, who is legal person, to provide a utility bill that matches with the name and address that is in company registration document, also we ask to provide a utility bill of one of the major shareholder or representative of a client matching with shareholder’s or representative’s name on its identification document.</li>
		</ol>

		<p>When you contact Nemondo, Nemondo may collect information sent to us through your computer, mobile phone, or other access device. This information may include your IP address, device information including, but not limited to, identifier, device name and type, operating system, location, mobile network information, and standard web log information, such as your browser type, traffic to and from our site, and the pages you accessed on our website. </p>

		<h4>Legal Basis for the Processing of Personal Data</h4>

		<p>Nemondo processes your personal data to provide our services to you and for one or more specific purposes in accordance with your consent.<br> You have right at any time to withdraw your consent to processing of personal data. The withdrawal of consent does not affect the legality of the processing done prior to the waiver of consent.</p>

		<h4>Personal Data We Share with Third Parties</h4>

		<p>While providing services to you it may be necessary for Nemondo to share your personal information with third parties. Nemondo does not share your personal information with third parties unless it is necessary to provide services to you or required by law. Nemondo may share your personal data with:</p>

		<ol>
			<li>identity verification services in order to prevent fraud. This allows Nemondo to confirm your identity by comparing the information you provide us to public records and other third-party databases. These service providers may create derivative data based on your personal data that can be used solely in connection with provision of identity verification and fraud prevention services;</li>
			<li>service providers under contract who help with parts of our business operations such as bill collection, marketing, and technology services. Our contracts require these service providers to only use your data in connection with the services they perform for us, and prohibit them from selling your data to anyone else. We provide to them only basic data such as name and email address, more sensitive date such as banking details, identifications documents, proof of address will not be shared to them;</li>
			<li>financial institutions with which we partner. Financial institutions will be provided only basic data to send and receive bank wires;</li>
			<li>companies or other entities that we plan to merge with or be acquired by. Should such a combination occur, we will require that the new combined entity follow this Privacy Policy with respect to your personal information. You will receive prior notice of any change in applicable policy;</li>
			<li>law enforcement agencies, officials or other third parties when:
				<ul>
					<li>we are compelled to do so by a subpoena, court order, or similar legal procedure;</li>
					<li>Nemondo is the victim of a fraud or other crime, data associated with the perpetrators may be shared with law enforcement.</li>
				</ul>
			</li>
			<li>other third parties with your consent or direction to do so.</li>
		</ol>

		<p>Third parties to whom we will transmit personal data, can be in the European Economic Area and outside of it in countries where the European Commission has not assessed the level of data protection or considered it to be sufficient by way of its adequacy decision. By accepting this Privacy Policy, you therefore understand and agree that upon disclosing personal data to Nemondo, this personal data can be transferred to and processed outside the European Economic Area.</p>

		<p>Due to the lack of adequate data protection levels, personal data may not be secured in these countries, including protection against any misuse, unauthorized access, disclosure, alteration or destruction, as guaranteed by the European Union.
		</p>

		<p>Nemondo will ensure that appropriate safeguards are applied when personal data is transferred to countries outside the European Economic Area.</p>

		<h4>How We Protect and Store Personal Data</h4>

		<p>Your personal data security is important for Nemondo. Nemondo has implemented the necessary organizational, physical and informational security measures to protect your personal data against any misuse, unauthorized access, disclosure, alteration or destruction. This applies, inter alia, to personal data transmitted abroad.</p>

		<p>Your personal data is received through our online platform, hosted under our AWS Cloud Server ​https://aws.amazon.com​ and is securely stored in our Amazon S3 buckets ​https://aws.amazon.com/s3/​. All personal data is automatically backed up on a regular basis. </p>

		<p>Your personal data is only accessible to authorized persons, who are obliged not to disclose your personal data to anyone.</p>

		<p>While you are a customer of Nemondo we will securely store your personal data till it is necessary for providing our services to you. Your personal data will be deleted when the Nemondo User Agreements is no longer valid, the need for the processing of such data has been exhausted or in any other case required by the law, including when you withdraw your consent to this privacy policy. In any case we will maintain anonymous data, including transaction data which is not directly linkable to you as a customer.</p>

		<h4>Automated Processing and Direct Marketing</h4>

		<p>Nemondo may use automated processing of your personal data for KYC procedures, which are necessary for entering into, or performance of, a contract between you and Nemondo.</p>

		<p>You agree that Nemondo may use automated processing (profiling) of your personal data such as number of transactions, average order size, location and type of client (natural or legal person) in order to design post sale marketing campaigns, to update you on our promotions and other updates and make offers to you (direct marketing).</p>

		<p>You can opt out any time from direct marketing and updates by click the “Unsubscribe” link in the footer of our e-mail.</p>

		<h4>Rights of the Data Subject Under this Privacy Policy </h4>

		<p>You may request information about your personal data, which we process, and submit requests for correcting any of your incorrect personal data. <br> Pursuant to applicable laws, you may request for the deletion or for limiting the processing of your personal data.<br> You may also object to our data processing, if you find that we process your personal data or parts of your personal data in violation of your rights.<br> You have the right to withdraw your consent to this Privacy Policy at any time. The withdrawal of consent shall not affect the lawfulness of processing based on consent before its withdrawal.<br> You have the right to apply to an appropriate data protection authority for remedy of your rights.</p>

		<h4>How We Use Cookies</h4>

		<p>When you access our website or content or Nemondo services, we or companies we work with may place small data files called cookies or pixel tags on your computer or other device. We use these technologies to:</p>

		<ul>
			<li>Recognize you as a Nemondo customer;</li>
			<li>Customize Nemondo Services, content, and advertising;</li>
			<li>Measure promotional effectiveness;</li>
		</ul>

		<p>Collect information about your computer or other access device to mitigate risk, help prevent fraud, and promote trust and safety.</p>

		<p>We may use both session and persistent cookies when you access our website or content. Session cookies expire and no longer have any effect when you log out of your account or close your browser. Persistent cookies remain on your browser until you erase them or they expire.</p>

		<p>We may also use Local Shared Objects, commonly referred to as "Flash cookies," to help ensure that your account security is not compromised, to spot irregularities in behavior to help prevent fraud, and to support our sites and services. We encode our cookies so that only we can interpret the information stored in them.</p>

		<p>You are free to decline our cookies if your browser or browser add-on permits, but doing so may interfere with your use of Nemondo website or services.</p>

		<p>We may periodically post changes to this Privacy Policy to the Nemondo website, and it is your responsibility to review this Privacy Policy frequently. When required by law, we will notify you of any changes to this Privacy Policy.</p>

		<p>If you have any questions about this policy, you may contact us at <a href="mailto:privacy@nemondo.com">privacy@nemondo.com</a></p>

	</div>

</main>

<?php include 'footer.php'; ?>

</body>
</html>