$(function(){

    setTimeout(function(){
        $('body').css({'opacity': '1'});
    }, 500);


    $(window).scroll(function() {
        var st = $(this).scrollTop();
        var ww = $(window).width();
        if(ww >=768){
            if(st >= 300) {
                $('.nem-s--purple').addClass('scrolled');
                $('.nem-s-middle').addClass('scrolled');
            } else {
                $('.nem-s--purple').removeClass('scrolled');
                $('.nem-s-middle').removeClass('scrolled');
            }
        }
    });

    //Modal initialization
    $(document).on('click', '.js-trigger-iFrame', function(event) {
        event.preventDefault();
        $('#modal-iFrame').iziModal('open', this); // Do not forget the "this"
    });

    $(".js_modal").animatedModal({
        color: 'rgba(0, 0, 0, .75)'
    });

    $('#nem-form').validator();

    $("#nem-form").submit(function(e) {
        e.preventDefault();
        var formData = $(this).serialize();
        var formMessages = $(this).find('.nem-result-info');
        $.ajax({
            url:     $(this).attr('action'),
            type:     'POST',
            data:     formData,
            success: function(response) {
                // Set the message text.
                formMessages.text(response);
                formMessages.removeClass('has-error');
                formMessages.addClass('has-success');

                // Clear the form.
                $('#name').val('');
                $('#email').val('');
                $('#org').val('');
            },
            error: function(data) {
                // Set the message text.
                formMessages.addClass('has-error');
                if (data.responseText !== '') {
                    formMessages.text(data.responseText);
                } else {
                    formMessages.text('Oops! An error occured and your message could not be sent.');
                }
            }
        });
        return false;
    });

});