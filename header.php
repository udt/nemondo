<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="format-detection" content="telephone=no">

	<meta name="description" content="">
	<meta name="keywords" content="">

	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="images/apple-touch-icon-152x152.png" />

	<link rel="icon" type="image/png" href="images/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="images/favicon-16x16.png" sizes="16x16" />

	<link rel="icon" type="image/x-icon" href="images/favicon.ico" />

	<title>Nemondo | Terms of Service</title>

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

	<link rel="stylesheet" href="css/libs.css">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>