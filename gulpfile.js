// Gulp.js configuration
'use strict';

const

  // source and build folders
  dir = {
    src         : 'src/',
    node_modules: 'node_modules/',
    build       : '../nemondo.loc/'
  },

  // Gulp and plugins
  gulp          = require('gulp'),
  gutil         = require('gulp-util'),
  newer         = require('gulp-newer'),
  imagemin      = require('gulp-imagemin'),
  sass          = require('gulp-sass'),
  postcss       = require('gulp-postcss'),
  deporder      = require('gulp-deporder'),
  concat        = require('gulp-concat'),
  stripdebug    = require('gulp-strip-debug'),
  uglify        = require('gulp-uglify')
;

// Browser-sync
var browsersync = false;

// PHP settings
const php = {
  src           : dir.src + '/**/*.php',
  build         : dir.build
};

// image settings
const images = {
  src         : dir.src + 'images/**/*',
  build       : dir.build + 'images/'
};

// fonts settings
const fonts = {
    src         : [dir.src + 'fonts/**/*', dir.node_modules + 'font-awesome/fonts/**/*'],
    build       : dir.build + 'fonts/'
};

// JavaScript settings
const js = {
  src         : dir.src + 'js/**/*',
  build       : dir.build + 'js/',
  filename    : 'scripts.js'
};

// Browsersync options
const syncOpts = {
  proxy       : 'nemondo.loc',
  host        : 'nemondo.loc',
  files       : dir.build + '**/*',
  open        : 'external',
  // notify      : false,
  // ghostMode   : false,
  ui: {
    port: 3000
  }
};

// CSS settings
var css = {
  src         : dir.src + 'sass/style.sass',
  watch       : dir.src + 'sass/**/*',
  build       : dir.build + 'css',
  sassOpts: {
    outputStyle     : 'nested',
    imagePath       : images.build,
    precision       : 3,
    errLogToConsole : true
  },
  processors: [
    require('postcss-assets')({
      loadPaths: ['images/'],
      basePath: dir.build,
      baseUrl: '/index.php'
    }),
    require('autoprefixer')({
      browsers: ['last 2 versions', '> 2%']
    }),
    require('css-mqpacker'),
    require('cssnano')
  ]
};

// copy PHP files
gulp.task('php', () => {
  return gulp.src(php.src)
    .pipe(newer(php.build))
    .pipe(gulp.dest(php.build));
});

// image processing
gulp.task('images', () => {
  return gulp.src(images.src)
    .pipe(newer(images.build))
    .pipe(imagemin())
    .pipe(gulp.dest(images.build));
});

// fonts processing
gulp.task('fonts', () => {
    return gulp.src(fonts.src)
        .pipe(gulp.dest(fonts.build));
});

// CSS processing
gulp.task('css', ['images'], () => {
  return gulp.src(css.src)
    .pipe(sass(css.sassOpts))
    .pipe(postcss(css.processors))
    .pipe(gulp.dest(css.build))
    .pipe(browsersync ? browsersync.reload({ stream: true }) : gutil.noop());
});

// CSS processing
gulp.task('css-libs', ['images'], () => {
  return gulp.src([
      dir.node_modules + 'normalize.css/normalize.css',
      dir.node_modules + 'font-awesome/css/font-awesome.css',
      dir.node_modules + 'aos/dist/aos.css',
      dir.node_modules + 'animate.css/animate.css',
      dir.node_modules + 'bootstrap-4-grid/css/grid.css',
    ])
    .pipe(concat('libs.css'))
    .pipe(sass(css.sassOpts))
    .pipe(postcss(css.processors))
    .pipe(gulp.dest(css.build))
    .pipe(browsersync ? browsersync.reload({ stream: true }) : gutil.noop());
});

// JavaScript processing
gulp.task('js', () => {
  return gulp.src(js.src)
    .pipe(deporder())
    .pipe(concat(js.filename))
    .pipe(stripdebug())
    .pipe(uglify())
    .pipe(gulp.dest(js.build))
    .pipe(browsersync ? browsersync.reload({ stream: true }) : gutil.noop());
});

// JavaScript libs
gulp.task('js-libs', () => {
    return gulp.src([
        dir.node_modules + 'bootstrap-validator/dist/validator.js',
        dir.node_modules + 'aos/dist/aos.js',
        dir.src + 'libs/animatedModal.js'
    ])
    .pipe(concat(js.filename))
    .pipe(concat('libs.js'))
    .pipe(uglify())
    .pipe(gulp.dest(js.build))
    .pipe(browsersync ? browsersync.reload({ stream: true }) : gutil.noop());
});

// browser-sync
gulp.task('browsersync', () => {
  if (browsersync === false) {
    browsersync = require('browser-sync').create();
    browsersync.init(syncOpts);
  }
});

// watch for file changes
gulp.task('watch', ['browsersync'], () => {

  // page changes
  gulp.watch(php.src, ['php'], browsersync ? browsersync.reload : {});

  // image changes
  gulp.watch(images.src, ['images']);

    // CSS changes
  gulp.watch(css.watch, ['css']);

  // JavaScript main changes
  gulp.watch(js.src, ['js']);

});

// default task
gulp.task('default', ['build', 'watch']);

// run all tasks
gulp.task('build', ['php', 'fonts', 'css', 'css-libs', 'js', 'js-libs']);