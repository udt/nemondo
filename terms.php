<?php include "header.php"; ?>

<header class="nem-header">
    <div class="container">
        <div class="row align-content-center justify-content-between">
            <div class="col-sm-6 col-xs-12">
                <a href="index.php" class="nem-logo">
                    <img src="/images/logo_header_1.5x.svg" alt="Nemondo logo" class="img-responsive">
                </a>
            </div>
            <div class="col-sm-6 col-xs-12 text-right">
                <nav>
                    <ul class="nem-nav">
                        <li>
                            <a href="index.php" class="nem-link">Home</a>
                        </li>
                        <li>
                            <a href="privacy.php" class="nem-link">Privacy Policy</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>

<main class="nem-main nem-page">
    <div class="container">
        <h2 class="text-center">Terms of Service</h2>

        <p>These terms govern the access and use of the websites (“the service”) of Nemondo OU, its affiliates, or subsidiaries, (collectively “Nemondo”) or any of its applications.</p>

        <ol>
            <li>Nemondo makes no warranty regarding the content of the site and all content is provided as is.</li>
            <li>Any software, applications, or like code items are provided as is and Nemondo makes no warranty as to compatibility with any browser or operating system.</li>
            <li>If you create a Nemondo account, you are solely responsible for protecting your login information. Login information may not be given to any other person or entity.</li>
            <li>Any disputes regarding the use of Nemondo’s account or service will be governed by the laws of Estonia and may only be brought within Estonia.</li>
            <li>Nemondo is acting under authorizations issued by the Estonian Financial Intelligence Unit: (i) nr FVR000028 (provider of a service of exchanging a virtual currency against a fiat currency and vice versa) and (ii) nr FRK000021 (provider of a virtual currency wallet service). </li>
            <li>Nemondo is providing exchange and wallet service with regards to virtual currencies (as alternative payment instruments) only. Nemondo is not providing any service in relation to exchange, trading, investing, managing or depositing of financial instruments. </li>
            <li>The web page or service is not to be used for any illegal activity. </li>
            <li>User understands that without acceptance of Nemondo service agreement, use of this site does not enter Nemondo into any legal relationship with the user.</li>
            <li>Any statements made on the service herein are for information only and are provided without warranty by Nemondo.</li>
            <li>Nemondo reserves the right in its sole discretion to ban any user who is abusing Nemondo’s account or service or using the service in violation of any applicable law.</li>
        </ol>


    </div>

</main>

<?php include 'footer.php'; ?>

</body>
</html>